#!/bin/bash
#
# Author: Tyler Wrightson
# Date: 8/22/2017
# Author2: Mariusz Galus
# Date: 11/24/2019

ARG1="$1"
ARG2="$2"
DIR="$PWD/crt"

if [ -z $ARG1 ]; then
	echo -e "Usage: crt.sh [query] <keyword>"
	echo -e "    <keyword> should be domain name with % for wildcard ex. %api%.example.com"
	echo -e "    [query] is optional arg can be provided to download ALL certificates into \$PWD/crt"
	exit
fi

if [ -z $ARG2 ]; then
    if [ $ARG1 = "query" ]; then
    	echo -e "Usage: crt.sh [query] <keyword>"
        echo -e "    <keyword> should be domain name with % for wildcard ex. %api%.example.com"
        echo -e "    [query] is optional arg can be provided to download ALL certificates into \$PWD/crt"
    	exit
    fi
    jq=$(jq --version)
    if [[ $jq =~ "jq-1." ]]; then
        curl -s https://crt.sh/?q=$ARG1\&output=json | jq -r '.[].name_value'
        echo $stuff
        exit
    fi

    curl -s https://crt.sh/?Identity=$ARG1 > $PWD/crt.html
    cat $PWD/crt.html | grep -oP '(<TD>)(.*?)(</TD>)' | cut -d '<' -f2 | cut -d '>' -f2 | xargs -n1 | sort | uniq
    rm $PWD/crt.html
    exit
fi

echo -e "[+] Making directory $DIR"
mkdir $DIR
echo -e "[+] Downloading from https://crt.sh"
ARG2=${ARG2// /+}
echo -e "[+] url: https://crt.sh/?q=$ARG2"
curl -s https://crt.sh/?q=$ARG2 > $DIR/curl.txt
echo -e "[+] Saving Certificate IDs to $DIR/crt.ids"
cat $DIR/curl.txt | grep ?id= | cut -d \" -f5 | cut -d '<' -f1 | cut -d '>' -f2 >> $DIR/crt.ids

TOTAL=`wc -l $DIR/crt.ids`
echo -e "[+] Total Number of Certs: $TOTAL"
cat $DIR/crt.ids| while read line
do
   echo "[+] Downloading Certificate ID: $line"
   curl -s https://crt.sh/?id=$line > $DIR/$line.txt 
done

# Note that the (.*?) makes the search 'ungreedy' - which matches
# only the first occurence of the <BR> right after our search string
cat $DIR/* | grep -oP '(DNS)(.*?)(<BR>)' | cut -d ":" -f2 | cut -d "<" -f1 | sort -u >> $DIR/domains.txt

echo -e "[+] Domains saved to: $DIR/domains.txt"
echo -e "[+] Done"

