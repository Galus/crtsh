# crtsh

Basically and updated crt.sh from https://github.com/tdubs/crt.sh but with the default behavior of searching for identities rather than downloading all certs and grepping for ALL unique SANS.

# CRT.sh Certificate Search Recon Script

 This bash script makes it easy to quickly download and parse
 the output from https://crt.sh website. All you need to supply
 is the search term to query crt.sh for. Note that I've never had 
 any luck querying for the actual domain name.

# Example Usages:
```
# Search IDENTITIES (quick way to enumerate services)
crt.sh %api%.yahoo.com 

# Search all certificates (downloads all certs and gets all associated domains)
crt.sh query google
```

Make sure to httprobe them and also do more recon on censys - Fun Enumer8
Shodan, pywebscreenshot, aquatone, sublist3r, lazyrecon, dirsearch, 4BtJic9TUCI?t=2279
